from django import forms
from django.forms import ModelForm
from .models import Customer, Employee, Service, PriceList, Order, OrderComposition


class NewCustomerForm(ModelForm):
    class Meta:
        model = Customer
        fields = ('full_name', 'phone', 'passport')


class NewServiceForm(ModelForm):
    class Meta:
        model = Service
        fields = ('name',)


class NewPriceListForm(ModelForm):
    class Meta:
        model = PriceList
        fields = ('service', 'price')


class NewOrderForm(forms.Form):
    OPEN = 'Открыт'
    CLOSED = 'Закрыт'
    status_choices = [
        (OPEN, 'Открыт'),
        (CLOSED, 'Закрыт'),
    ]
    order_customer = forms.ModelChoiceField(queryset=Customer.objects.all(), label='Заказчик')
    order_date_schedule = forms.DateField(label='Дата планового выполнения')
    price_list = forms.ModelChoiceField(queryset=PriceList.objects.all(), label='Строка прейскуранта')
    count = forms.IntegerField(label='Количество')


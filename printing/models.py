from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User


# Create your models here.
OPEN = 'Открыт'
CLOSED = 'Закрыт'
status_choices = [
    (OPEN, 'Открыт'),
    (CLOSED, 'Закрыт'),
]


class Customer(models.Model):
    full_name = models.CharField(verbose_name='ФИО', max_length=50)
    phone = models.CharField(verbose_name='Номер телефона', max_length=11, null=True, blank=True)
    passport = models.CharField(verbose_name='Паспорт', max_length=10, null=True, blank=True)

    def __str__(self):
        return self.full_name

    def get_absolute_url(self):
        return reverse('customer-detail', args=[str(self.id), ])


class Employee(models.Model):
    user = models.OneToOneField(
        to=User,
        on_delete=models.CASCADE,
        blank=True, null=True,
    )

    def __str__(self):
        return self.user.get_full_name()


class Service(models.Model):
    name = models.CharField(verbose_name='Наименование', max_length=50)

    def __str__(self):
        return self.name


class PriceList(models.Model):
    service = models.ForeignKey(
        to=Service,
        verbose_name='Услуга',
        on_delete=models.CASCADE,
    )
    price = models.PositiveIntegerField(verbose_name='Цена')

    def __str__(self):
        return f'{self.service} - {self.price} руб'


class Order(models.Model):
    customer = models.ForeignKey(
        to=Customer,
        verbose_name='Заказчик',
        on_delete=models.CASCADE,
    )
    employee = models.ForeignKey(
        to=Employee,
        verbose_name='Сотрудник',
        on_delete=models.CASCADE,
    )
    date_admission = models.DateField(verbose_name='Дата приема')
    date_schedule = models.DateField(verbose_name='Дата планового выполнения')
    date_closed = models.DateField(
        verbose_name='Дата закрытия заказа',
        blank=True, null=True
    )
    status = models.CharField(verbose_name='Статус', choices=status_choices, max_length=15, default=OPEN)

    def __str__(self):
        return f'{self.pk} - {self.customer.full_name} - {self.date_admission}'


class OrderComposition(models.Model):
    order = models.ForeignKey(
        to=Order,
        verbose_name='Заказ',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    price_list = models.ForeignKey(
        to=PriceList,
        verbose_name='Строка прейскуранта',
        on_delete=models.CASCADE,
    )
    count = models.PositiveIntegerField(verbose_name='Количество услуг')

    def __str__(self):
        return str(f'{self.pk} - {self.price_list.service.name} - {self.count}')

    def get_absolute_url(self):
        return reverse('order-detail', args=[str(self.id), ])

    def get_total_cost(self):
        total_cost = self.price_list.price * self.count
        return total_cost

from datetime import datetime
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views import generic
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Customer, Employee, Service, PriceList, Order, OrderComposition, CLOSED
from .forms import NewCustomerForm, NewServiceForm, NewPriceListForm, NewOrderForm
from django.contrib.auth.decorators import login_required


# Create your views here.
def index(request):
    num_orders = OrderComposition.objects.all().count()
    num_services = Service.objects.all().count()
    num_customers = Customer.objects.all().count()
    return render(
        request,
        'index.html',
        context={
            'num_orders': num_orders,
            'num_services': num_services,
            'num_customers': num_customers,
        }
    )


class CustomerView(LoginRequiredMixin, generic.ListView):
    model = Customer
    context_object_name = 'customer_list'
    template_name = 'customer_list.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            customer_list = Customer.objects.filter(full_name__icontains=query)
        else:
            customer_list = Customer.objects.all()
        return customer_list


class CustomerDetailView(LoginRequiredMixin, generic.DetailView):
    model = Customer
    template_name = 'customer_detail.html'


class EmployeeView(LoginRequiredMixin, generic.ListView):
    model = Employee
    context_object_name = 'employee_list'
    template_name = 'employee_list.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            employee_list = Employee.objects.filter(
                Q(user__first_name__icontains=query) |
                Q(user__last_name__icontains=query) |
                Q(user__username__icontains=query)
            )
        else:
            employee_list = Employee.objects.all()
        return employee_list


class PriceListView(LoginRequiredMixin, generic.ListView):
    model = PriceList
    context_object_name = 'price_list'
    template_name = 'price_list.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            price_list = PriceList.objects.filter(Q(service__name__icontains=query) | Q(price__icontains=query))
        else:
            price_list = PriceList.objects.all()
        return price_list


class ServiceListView(LoginRequiredMixin, generic.ListView):
    model = Service
    context_object_name = 'service_list'
    template_name = 'service_list.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            service_list = Service.objects.filter(name__icontains=query)
        else:
            service_list = Service.objects.all()
        return service_list


class OrderCompositionListView(LoginRequiredMixin, generic.ListView):
    model = OrderComposition
    context_object_name = 'order_list'
    template_name = 'order_list.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        employee_id = self.request.GET.get('employee_id')
        if employee_id:
            employee = Employee.objects.filter(pk=employee_id).first()
            context['employee'] = employee
        return context

    def get_queryset(self):
        query = self.request.GET.get('q')
        employee_id = self.request.GET.get('employee_id')
        if employee_id:
            order_list = OrderComposition.objects.filter(order__employee__pk=employee_id)
        else:
            if query:
                order_list = OrderComposition.objects.filter(
                    Q(order__customer__full_name__icontains=query) |
                    Q(order__date_admission__icontains=query) |
                    Q(order__status__icontains=query) |
                    Q(price_list__service__name__icontains=query) |
                    Q(order__status__icontains=query)
                )
            else:
                order_list = OrderComposition.objects.all()
        return order_list


class OrderDetailView(LoginRequiredMixin, generic.DetailView):
    model = OrderComposition
    context_object_name = 'order'
    template_name = 'order_detail.html'


@login_required
def create_new_customer(request):
    customer = Customer()
    if request.method == 'POST':
        form = NewCustomerForm(request.POST)
        if form.is_valid():
            customer.full_name = form.cleaned_data['full_name']
            customer.phone = form.cleaned_data['phone']
            customer.passport = form.cleaned_data['passport']
            customer.save()
            return HttpResponseRedirect(reverse('customer-list'))
    else:
        form = NewCustomerForm()

    return render(request, 'create_customer.html', {'form': form})


@login_required
def create_new_service(request):
    service = Service()
    if request.method == 'POST':
        form = NewServiceForm(request.POST)
        if form.is_valid():
            service.name = form.cleaned_data['name']
            service.save()
            return HttpResponseRedirect(reverse('service-list'))
    else:
        form = NewServiceForm()

    return render(request, 'create_service.html', {'form': form})


@login_required
def create_new_price_list(request):
    price_list = PriceList()
    if request.method == 'POST':
        form = NewPriceListForm(request.POST)
        if form.is_valid():
            price_list.service = form.cleaned_data['service']
            price_list.price = form.cleaned_data['price']
            price_list.save()
            return HttpResponseRedirect(reverse('price-list'))
    else:
        form = NewPriceListForm()

    return render(request, 'create_price_list.html', {'form': form})


@login_required
def create_new_order(request):
    order_composition = OrderComposition()
    order = Order()
    employee = Employee.objects.filter(user=request.user).first()
    if request.method == 'POST':
        form = NewOrderForm(request.POST)
        if form.is_valid():
            order.customer = form.cleaned_data['order_customer']
            order.employee = employee
            order.date_admission = datetime.today()
            order.date_schedule = form.cleaned_data['order_date_schedule']
            order.save()
            order_composition.price_list = form.cleaned_data['price_list']
            order_composition.count = form.cleaned_data['count']
            order_composition.order = order
            order_composition.save()
            return HttpResponseRedirect(reverse('order-list'))
    else:
        form = NewOrderForm()

    return render(request, 'create_order.html', {'form': form})


@login_required
def close_order(request, pk):
    order_composition = OrderComposition.objects.filter(pk=pk).first()
    order = order_composition.order
    order.status = CLOSED
    order.date_closed = datetime.today()
    order.save()
    order.refresh_from_db()
    order_composition.refresh_from_db()
    print(order_composition.order.status)
    return HttpResponseRedirect(reverse('order-detail', args=[order_composition.pk]))


@login_required
def employee_orders(request, pk):
    urlparams = f'?employee_id={pk}'
    return HttpResponseRedirect(reverse('order-list')+urlparams)



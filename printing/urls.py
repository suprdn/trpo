from django.urls import path, re_path
from django.views.generic import RedirectView

from . import views

urlpatterns = [
    path('', RedirectView.as_view(url='main/', permanent=True)),
    re_path(r'main/$', views.index, name='index'),
    re_path(r'customers/$', views.CustomerView.as_view(), name='customer-list'),
    re_path(r'customer/(?P<pk>\d+)$', views.CustomerDetailView.as_view(), name='customer-detail'),
    re_path(r'employees/$', views.EmployeeView.as_view(), name='employee-list'),
    re_path(r'employee/(?P<pk>\d+)$', views.employee_orders, name='employee-orders'),
    re_path(r'pricelist/$', views.PriceListView.as_view(), name='price-list'),
    re_path(r'services/$', views.ServiceListView.as_view(), name='service-list'),
    re_path(r'orders/$', views.OrderCompositionListView.as_view(), name='order-list'),
    re_path(r'order/(?P<pk>\d+)$', views.OrderDetailView.as_view(), name='order-detail'),
    re_path(r'newcustomer/$', views.create_new_customer, name='create-customer'),
    re_path(r'newservice/$', views.create_new_service, name='create-service'),
    re_path(r'neworder/$', views.create_new_order, name='create-order'),
    re_path(r'newpricelist/$', views.create_new_price_list, name='create-price-list'),
    re_path(r'order/(?P<pk>[-\w]+)/close/$', views.close_order, name='close-order'),
]